#!/bin/sh

chown app -R .

# DATABASE_URL contains the information to connect to PostgreSQL. But we need to change it to the jdbc format
JDBC_URL=$(echo $DATABASE_URL | sed -r 's/^postgres:\/\/([0-9a-zA-Z_-]+):([^@]+)@([0-9a-zA-Z_.-]+):([[:digit:]]+)\/([0-9a-zA-Z_-]+)/jdbc:postgresql:\/\/\3:\4\/\5?user=\1\&password=\2/')

export SPRING_APPLICATION_JSON="{ \"server\": { \"address\":\"0.0.0.0\", \"port\":5000, \"context-path.\": \"\" }, \"spring\": { \"datasource\": { \"url\": \"${JDBC_URL}\", \"driver-class-name\": \"org.postgresql.Driver\" }, \"jpa\": { \"properties\": { \"hibernate\": { \"dialect\": \"org.hibernate.dialect.PostgreSQL94Dialect\" } } } } }"
echo $SPRING_APPLICATION_JSON

sudo -EHnu app -- java -jar $(ls webgoat-server/target/webgoat-server-*jar)
